-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-04-2017 a las 14:06:14
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bh`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_administrador` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellido` varchar(300) NOT NULL,
  `correo` varchar(300) NOT NULL,
  `nivel` int(11) NOT NULL,
  `pw` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_administrador`, `nombre`, `apellido`, `correo`, `nivel`, `pw`) VALUES
(1, 'yonathan', 'suarez', 'y@y.com', 1, '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE `galeria` (
  `id_galeria` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `slider` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id_galeria`, `nombre`, `titulo`, `slider`) VALUES
(25, 'Piloto ROV clase 1 observaciÃ³n', 'galeria_garr.jpg', 1),
(26, 'buzo con aire enriquesido', 'galeria_image_1489586229FLVmgJ4PeMWuWC8w.jpg', 1),
(27, 'Buzo tender: OCDL1', 'galeria_image_1489586248XIGBi4a1ktMV0CXi.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id_modulo` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `fondo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id_modulo`, `nombre`, `fondo`) VALUES
(42, 'EMPRESA', 'fondo_logobuzo.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagina`
--

CREATE TABLE `pagina` (
  `id_pagina` int(11) NOT NULL,
  `portada` varchar(1000) NOT NULL,
  `logo` varchar(1000) NOT NULL,
  `face` varchar(100) NOT NULL,
  `twiter` varchar(100) NOT NULL,
  `footer` varchar(500) NOT NULL,
  `link` varchar(300) NOT NULL,
  `favicon` varchar(1000) NOT NULL,
  `fuente` varchar(400) NOT NULL,
  `instagran` varchar(300) NOT NULL,
  `correo` varchar(300) NOT NULL,
  `direccion` varchar(350) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pagina`
--

INSERT INTO `pagina` (`id_pagina`, `portada`, `logo`, `face`, `twiter`, `footer`, `link`, `favicon`, `fuente`, `instagran`, `correo`, `direccion`, `telefono`) VALUES
(12, 'portada.jpg', 'logo.png', 'a', 'b', '8', '7', 'favicon.jpg', '2', 'c', '4@a.com', '6', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessiones`
--

CREATE TABLE `sessiones` (
  `id_sessiones` int(11) NOT NULL,
  `title_sessiones` varchar(500) NOT NULL,
  `contenido` varchar(500) NOT NULL,
  `link` varchar(500) NOT NULL,
  `posicion_y` int(11) NOT NULL,
  `fondo` varchar(500) DEFAULT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `posicion_x` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sessiones`
--

INSERT INTO `sessiones` (`id_sessiones`, `title_sessiones`, `contenido`, `link`, `posicion_y`, `fondo`, `imagen`, `style`, `posicion_x`) VALUES
(8, 'BUCEO', 'Ingresamos a las profundidades de cada problema para hallar una solución y así satisfacer a cada uno de nuestros clientes.', '#', 0, NULL, 'buzo.png', NULL, NULL),
(9, 'CAPACITACIÓN', 'Deseamos más personal capacitado en el mundo del buceo industrial y científico, necesario para suplir con los requerimientos continuos exigidos por nuestros clientes.', '#', 1, 'blue', 'buzo-con-luz.png', 1, 2),
(10, 'PROYECTOS', 'La calidad y la excelencia de nuestros servicios se visualizan en cada uno de los testimonios de nuestros exclusivos clientes.', '#', 2, 'video', NULL, 1, 1),
(11, 'EQUIPOS', 'Existen circunstancias en las cuales no se debe arriesgar a nuestro personal; es por ello que poseemos equipos de última tecnología para resguardar la seguridad de nuestros buzos.', '#', 3, NULL, 'rov.png', 1, 3),
(12, 'EMPRESA', 'Contamos con los mejores profesionales en el área, capacitados y dispuestos a garantizar los resultados.', '#', 4, NULL, 'subarinosinluces.png', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `submodulos`
--

CREATE TABLE `submodulos` (
  `id_submodulos` int(11) NOT NULL,
  `titulo_sub` varchar(500) NOT NULL,
  `icono` varchar(500) NOT NULL,
  `contenido` varchar(1000) NOT NULL,
  `imagen_sub` varchar(500) NOT NULL,
  `id_modulos_id_sub` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `submodulos`
--

INSERT INTO `submodulos` (`id_submodulos`, `titulo_sub`, `icono`, `contenido`, `imagen_sub`, `id_modulos_id_sub`) VALUES
(16, 'RESEÑA', 'logo_RESEÑAhola.png', 'Eco Servicios Terraquática 2009, C.A. Inicia sus actividades y operaciones de buceo industrial en el año 2001 en uno de los lugares más exigentes a nivel técnico en el país, “La Represa del Guri”. Definiéndose por una filosofía de trabajo orientada a la satisfacción del cliente. Conformada por profesionales altamente capacitados y certificados en el área del buceo industrial y científico, encargándose de prestar asesoría y servicios subacuáticos a nivel nacional.\r\n\r\nContando con más de 16 años de experiencia en el ramo, nuestro objetivo principal es brindar una solución integral a las problemáticas surgidas en el entorno subacuático.\r\n\r\nEs importante resaltar, que nuestro talento humano siempre se encuentra en continua formación, certificados actualmente por el Instituto Nacional de los Espacios Acuáticos (INEA).\r\n\r\nIgualmente, nuestros servicios se ven fortalecidos por contar con un stock permanente de equipos, dispuestos a suplir sus necesidades en tiempo récord.', 'submodulo_RESEÑAlogobuzo.png', 42),
(17, 'MISIÓN', 'logo_MISIÓNbarco.png', 'Nuestra misión constante como profesionales capacitados en buceo industrial y científico es brindarles a nuestros clientes la mejor asesoría  y capacitación, bajo los más altos estándares de calidad.  Proporcionar los equipos adecuados para disminuir los riesgos inherentes, contando para ello con mecanismos y dispositivos de vanguardia. Garantizando la optimización del trabajo subacuático.', 'submodulo_MISIÓNbuzopiedra.png', 42),
(18, 'Visión', 'logo_Visión67943.png', 'Nos proyectamos por ser la compañía de referencia en el mercado venezolano y países limítrofes, líder en soluciones innovadoras, certificada en el área del buceo industrial y científico. Reconocida por el compromiso estricto del cumplimiento de sus objetivos.', 'submodulo_Visiónimage_1489586310sAoYoqan1uWVEjcD.jpg', 42),
(19, 'Valores', 'logo_Valoresstar_favorite_5754.png', 'Compromiso.\r\nResponsabilidad.\r\nDisciplina.\r\nÉtica profesional.\r\nCapacitación.\r\nInnovación.', 'submodulo_Valoresimage_1489586297BxkcDWIsBcwXSEMe.jpg', 42);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_administrador`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id_galeria`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id_modulo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `pagina`
--
ALTER TABLE `pagina`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indices de la tabla `sessiones`
--
ALTER TABLE `sessiones`
  ADD PRIMARY KEY (`id_sessiones`);

--
-- Indices de la tabla `submodulos`
--
ALTER TABLE `submodulos`
  ADD PRIMARY KEY (`id_submodulos`),
  ADD KEY `id_modulos_id_sub` (`id_modulos_id_sub`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_administrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id_galeria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id_modulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `pagina`
--
ALTER TABLE `pagina`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `sessiones`
--
ALTER TABLE `sessiones`
  MODIFY `id_sessiones` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `submodulos`
--
ALTER TABLE `submodulos`
  MODIFY `id_submodulos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
